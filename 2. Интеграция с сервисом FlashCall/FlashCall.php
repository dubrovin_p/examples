<?php
namespace Machaon;

class FlashCall
{
    const URL = "https://online.sigmasms.ru/api/";
    const LOGIN = "*****";
    const PASS = "*****";
    const SENDER = "Clever";

    protected $token = "";
    protected $code = "";

    public function __construct()
    {
        $this->getToken();
        $this->getCode();
    }

    protected function getCode(): void
    {
        $this->code = substr(time(), -4);
        $_SESSION['AUTH_CODE'] = $this->code;
    }

    public static function checkCode(string $code): bool
    {
        if ($_SESSION['AUTH_CODE'] && $_SESSION['AUTH_CODE'] == $code) {
            return true;
        }
        return false;
    }

    protected function getToken(): void
    {
        $fields = [
            'username' => self::LOGIN,
            'password' => self::PASS
        ];
        if ($res = $this->sendCurl("login", $fields)) {
            $res = json_decode($res);
            if ($res->token) {
                $this->token = $res->token;
            } else {
                 throw new Exception("No token");
            }
        } else {
            throw new Exception("No login", 1);
        }
    }

    /**
     * @param  string  $phone
     */
    public function getCall(string $phone)
    {
        $fields = [
            "recipient" => $phone,
            "type" => "flashcall",
            "payload" => [
                "sender" => self::SENDER,
                "text" => $this->code
            ]
        ];
        if ($res = $this->sendCurl("sendings", $fields,  ["Accept: application/json", "Authorization: ".$this->token])) {
            $res = json_decode($res);
            if (!$res->id) {
                throw new Exception("Call error", 1);
            }
            return $res;
        }

        throw new Exception("Call error", 1);
    }

    /**
     * @param  string  $url
     * @param  array  $fields
     * @param  array  $header
     * @return bool|string
     */
    public function sendCurl(string $url, array $fields = [], array $header = []): ?string
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::URL . $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array_merge(["Content-Type: application/json; charset=UTF-8"], $header));
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode( $fields ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);  
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $response = curl_exec($ch);
        curl_close($ch);

        if (!$response) {
            return null;
        }

        return $response;
    }
}
