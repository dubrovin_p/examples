<?php
if($_REQUEST['NUMBERMASK']) {
    $inMask = strtolower($_REQUEST['NUMBERMASK']);
    $arrMask = str_split($inMask);
    $chars = [];
    $cifer = [];
    $ciferKey = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'];
    $i = 0;
    foreach($arrMask as $char) {
        if (!is_numeric($char) && !in_array($char, $chars)) {
            $chars[] = $char;
        } elseif (is_numeric($char) && !in_array($char, $cifer)) {
            $cifer[$ciferKey[$i]] = $char;
            $inMask = str_replace($char, $ciferKey[$i], $inMask);
            $i++;
        }
    }
    $numbers = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
    $masksFilter = [$inMask];
    foreach($chars as $char) {
        $masks = [];
        foreach ($numbers as $number) {
            foreach ($masksFilter as $mask) {
                if (strpos($mask, $number)===false)
                    $masks[] = str_replace($char, $number, $mask);
            }
        }
        $masksFilter = $masks;
    }
    foreach($cifer as $id=>$c) {
        foreach($masksFilter as &$mask) {
            $mask = str_replace($id, $c, $mask);
        }
    }
    $db = CIBlockElement::GetList(
        [],
        ['IBLOCK_ID' => IBLOCK_CATALOG_NUMBER,
            [
                'LOGIC'=>'OR',
                ['%NAME'=> $masksFilter],
                ['%PROPERTY_FED_NUMBER'=> $masksFilter]
            ]
        ],
        false,
        false,
        ['ID']
    );
    while($number = $db->Fetch()) {
        $arNumber[] = $number['ID'];
    }
    if(!$arNumber) {
        $arNumber = false;
    }
}