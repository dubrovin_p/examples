<?php
namespace Machaon\B2B;

use Bitrix\Main\Config\Option;

class Mailer
{
    /**
     * Разрешенные к загрузке форматы файлов
     */
    const ALLOWED_FORMATS = ['VND.MS-EXCEL', 'VND.OPENXMLFORMATS-OFFICEDOCUMENT.SPREADSHEETML.SHEET', 'ZIP', 'GZIP'];

    /**
     * Возможные форматы архивов
     */
    const ARCHIVE_FORMATS = ['ZIP', 'GZIP'];

    /** @var string Папка для загрузки файлов */
    protected $uploadDir;

    /** @var array Список дилеров с ключевыми фразами */
    protected $dealers;

    /** @var array Настройки почтового соединения */
    protected $mailSettings;

    /** @var object Поток IMAP */
    protected $imap;

    public function __construct()
    {
        if (!extension_loaded('imap')) {
            throw new \Exception('IMAP not available!');
        }
        $this->uploadDir = Option::get("hitech.pricesparser", "hitech_pricesparser_main_path");
        self::existDir($_SERVER['DOCUMENT_ROOT'].$this->uploadDir);
        $this->dealers = $this->GetDealers();
        $this->mailSettings = [
            'host' => Option::get("hitech.pricesparser", "hitech_pricesparser_email_host"),
            'port' => Option::get("hitech.pricesparser", "hitech_pricesparser_email_port"),
            'login' => Option::get("hitech.pricesparser", "hitech_pricesparser_email_login"),
            'pass' => Option::get("hitech.pricesparser", "hitech_pricesparser_email_pass")
        ];
        if (!$this->mailSettings['host'] || !$this->mailSettings['port'] || !$this->mailSettings['login'] || !$this->mailSettings['pass']) {
            throw new \Exception('Email settings not found!');
        }
        $this->imap =imap_open("{{$this->mailSettings['host']}:{$this->mailSettings['port']}/imap/ssl/novalidate-cert}INBOX", $this->mailSettings['login'], $this->mailSettings['pass']);
    }

    /**
     * Получает почту с указанного в настройках ящика. Сохраняет вложенные файлы в папку на сервере
     */
    public function readMail()
    {
        $mailsId = imap_search($this->imap, 'ALL');
        $error = imap_errors();
        if ($error) {
            throw new \Exception(var_export($error));
        }
        foreach ($mailsId as $num) {
            $header = imap_header($this->imap, $num);
            $header = json_decode(json_encode($header), true);
            $msgStructure = imap_fetchstructure($this->imap, $num);
            if(isset($msgStructure->parts)) {
                for($j = 1, $f = 2; $j < count($msgStructure->parts); $j++, $f++) {
                    if (in_array($msgStructure->parts[$j]->subtype, self::ALLOWED_FORMATS) || $msgStructure->parts[$j]->subtype=='OCTET-STREAM') {
                        $mails_data[$num]["attachs"][$j]["name"] = $this->getImapTitle($msgStructure->parts[$j]->parameters[0]->value);
                        $mails_data[$num]["attachs"][$j]["file"] = $this->structureEncoding(
                            $msgStructure->parts[$j]->encoding,
                            imap_fetchbody($this->imap, $num, $f)
                        );
                        $fileName = iconv("windows-1251", "utf-8", $mails_data[$num]["attachs"][$j]["name"]);
                        $curDealer = '';
                        foreach ($this->dealers as $dealer) {
                            if (stripos($mails_data[$num]["attachs"][$j]["name"], $dealer['KEY'])!==false) {
                                $curDealer = $dealer['NAME'];
                            }
                        }
                        if (!$curDealer) continue;
                        $dir = \Cutil::translit($curDealer,"ru",["replace_space"=>"-","replace_other"=>"-"]).'/';
                        $filePath = $_SERVER['DOCUMENT_ROOT'].$this->uploadDir.$dir;
                        self::existDir($filePath, true);
                        file_put_contents($filePath.$fileName, $mails_data[$num]["attachs"][$j]["file"]);
                        imap_mail_move($this->imap, $num, 'completed');
                        imap_expunge($this->imap);
                        if ($this->isArchive($msgStructure->parts[$j]->subtype)) {
                            $arUnpackOptions = Array(
                                "REMOVE_PATH"      => $_SERVER["DOCUMENT_ROOT"],
                                "UNPACK_REPLACE"   => false
                            );
                            $resArchiver = \CBXArchive::GetArchive($filePath.$fileName);
                            $resArchiver->SetOptions($arUnpackOptions);
                            $uRes = $resArchiver->Unpack($filePath);
                            if ($uRes) {
                                unlink($filePath.$fileName);
                            }
                        }
                    }
                }
            }
        }
        imap_close($this->imap);
    }

    /**
     * Создает папку, если она отсутствует
     * @param $dir - заголовок вложения
     * @param $clear - запускать ли очистку
     */
    public static function existDir(string $dir, bool $clear = false)
    {
        if (!is_dir($dir)) {
            if (!mkdir($dir, BX_DIR_PERMISSIONS, true)) {
                throw new \Exception(sprintf('Invoice %s not found', $dir));
            }
        }
        if ($clear) {
            self::clearDir($dir);
        }
    }

    /**
     * Получает название вложения в установленной кодировке
     * @param $str - заголовок вложения
     * @return string
     */
    private function getImapTitle(string $str) : string
    {
        $mime = imap_mime_header_decode($str);
        $title = "";
        foreach($mime as $key => $m) {
            if(!$this->checkCp1251($m->charset)) {
                $title .= $this->convertToCp1251($m->charset, $m->text);
            } else {
                $title .= $m->text;
            }
        }
        return $title;
    }

    /**
     * Сверяет кодировку c windows-1251
     * @param $charset - проверяемая кодировка
     * @return bool
     */
    private function checkCp1251(string $charset) : bool
    {
        if(strtolower($charset) != "windows-1251" && strtolower($charset) != "default") {
            return false;
        }
        return true;
    }

    /**
     * Приводит строку к кодировке windows-1251
     * @param $in_charset - исходная кодировка
     * @param $str - конвертируемая строка
     * @return string
     */
    private function convertToCp1251(string $in_charset, string $str) : string
    {
        return iconv(strtolower($in_charset), "windows-1251", $str);
    }

    /**
     * Конвертирует тело письма
     * @param $encoding - исходная кодировка
     * @param $msg_body - конвертируемая строка
     * @return string
     */
    private function structureEncoding(string $encoding, string $msg_body) : string
    {
        switch((int) $encoding) {
            case 4:
                $body = imap_qprint($msg_body);
                break;
            case 3:
                $body = imap_base64($msg_body);
                break;
            case 2:
                $body = imap_binary($msg_body);
                break;
            case 1:
                $body = imap_8bit($msg_body);
                break;
            case 0:
                $body = $msg_body;
                break;
            default:
                $body = "";
                break;
        }
        return $body;
    }

    /**
     * Проверяет является ли вложение архивом
     * @param $type - тип файла из заголовка
     * @return bool
     */
    public function isArchive(string $type) : bool
    {
        return in_array($type, self::ARCHIVE_FORMATS);
    }

    /**
     * Получает настройки почтовых дилеров
     * @return array
     */
    public static function GetDealers() : array
    {
        return (unserialize(Option::get("hitech.pricesparser", "hitech_pricesparser_email_dealers")))?:[];
    }

    /**
     * Очищает папку перед использованием
     * @return array
     */
    private static function clearDir($dir) {
        if (file_exists($dir)) {
            foreach (glob($dir.'*') as $file) {
                unlink($file);
            }
        }
    }
}