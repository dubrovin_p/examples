<?
namespace Machaon\B2B;

use \Bitrix\Main\Config\Option;

class PricesParser
{
    /** @var string Полный путь к файлу */
    protected $file;

    /** @var string ID склада поставщика */
    protected $storeID;

    /** @var array Коды полей и свойств, по которым осуществляется идентификация товаров в этом прайсе */
    protected $needs;

    /** @var array Массив товаров, сгруппированных по полям-идентификаторам */
    protected $goods;

    /** @var array Массив товаров, сгруппированных по вариантам */
    protected $vars;

    /** @var array Массив товаров, сгруппированных по вариантам-маскам */
    protected $vvars;

    /** @var array Настройки обработки прайс-листа */
    protected $settings;

    /** @var array Массив ID обновленных товаров */
    protected $ids = [];

    /** @var array Массив количества товаров
     Нужен в случае, если товар в прайсе встречается как под 1 идентификатором, так и под его вариантами:
     Например, товар с артикулом 43UH5F на сайте встречается в прайсе под артикулами 43UH5F-H и 43UH5F-B и на сайт должна попасть сумма их остатков.
     */
    protected $q = [];

    /**
     * @param $file - полный путь к файлу
     * @param $storeID - ID склада поставщика
     * @param $settings - Настройки обработки прайс-листа
     * @throws \Exception - Пустые настройки, несуществующий активный склад, недоступный файл
     */
    public function __construct(string $file, int $storeID, array $settings)
    {
        if (!class_exists('PHPExcel')) {
            require_once(__DIR__."/PHPExcel/PHPExcel.php");
        }
        \Bitrix\Main\Loader::includeModule('catalog');
        $rsStore = \Bitrix\Catalog\StoreTable::getList([
            'filter' => ['ACTIVE' => 'Y', 'ID' => $storeID],
        ]);
        if($arStore=$rsStore->fetch()) {
            $this->storeID = $storeID;
        } else {
            throw new \Exception('Store not found');
        }
        if (file_exists($file)) {
            $this->file = $file;
        } else {
            throw new \Exception('File not found');
        }
        if ($settings) {
            $this->settings = $settings;
        } else {
            throw new \Exception('Settings for file not found');
        }
        $this->loadCodes();
        $this->loadGoods();
    }

    /**
     * Обрабатывает файл согласно настройкам
     * @throws \Exception - Количество листов отличается от указанного в настройках
     */
    public function parseFile()
    {
        if (!$this->settings['SKIP']) {
            $excel = \PHPExcel_IOFactory::load($this->file);
            foreach($excel ->getWorksheetIterator() as $l=>$worksheet) {
                if ($this->settings['SHEETS'][$l] && !$this->settings['SHEETS'][$l]['SKIP']) {
                    foreach($worksheet->toArray() as $i=>$row) {
                        foreach ($this->settings['SHEETS'][$l]['ARTICUL'] as $articul) {
                            $ident = iconv('UTF-8', 'windows-1251', $row[$articul]);
                            $find = false;
                            foreach ($this->needs as $need) {
                                if ($this->goods[$need][$ident]) {
                                    $find = $this->goods[$need][$ident];
                                    break;
                                }
                            }
                            if (!$find) {
                                if ($this->vars[$ident]) {
                                    $find = $this->vars[$ident];
                                } else {
                                    foreach($this->vvars as $vvar=>$id) {
                                        if (stripos($ident, $vvar)===0) {
                                            $find = $id;
                                        }
                                    }
                                }
                            }
                            if ($find) {
                                foreach ($this->settings['SHEETS'][$l]['QUANTITY'] as $quantity) {
                                    $q = iconv('UTF-8', 'windows-1251', $row[$quantity]);
                                    if ($q) {
                                        if ($this->settings['SHEETS'][$l]['CHANGES'][$q]) {
                                            $q = $this->settings['SHEETS'][$l]['CHANGES'][$q];
                                        }
                                        if (!$this->q[$find]) {
                                            $this->q[$find] = 0;
                                        }
                                        $this->q[$find] += intval($q);
                                        $this->quantityUpdate($find, $this->q[$find]);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            $this->clearRests();
        }
    }

    /**
     * Формирует массив кодов полей и свойств ИБ, которые используются для идентификации в этом прайс-листе
     */
    protected function loadCodes()
    {
        foreach ($this->settings['IDENT'] as $ident) {
            $code = explode(' / ', $ident);
            if (!in_array($code[0], $this->needs)) $this->needs[] = $code[0];
        }
    }

    /**
     * Формирует массив товаров с разбивкой по идентификаторам
     */
    protected function loadGoods()
    {
        if ($this->needs) {
            $res = \CIBlockElement::GetList([], ['IBLOCK_ID' => Option::get('hitech.pricesparser', 'hitech_pricesparser_main_iblock'), 'ACTIVE' => 'Y', 'SECTION_GLOBAL_ACTIVE' => 'Y', 'ACTIVE_DATE' => 'Y'], false, false);
            while($el = $res->GetNextElement()) {
                $f = $el->GetFields();
                $p = $el->GetProperties();
                foreach ($this->needs as $need) {
                    if ($f[$need]) {
                        $this->goods[$need][$f[$need]] = $f['ID'];
                    } elseif($p[$need]['VALUE']) {
                        if (is_array($p[$need]['VALUE'])) {
                            foreach ($p[$need]['VALUE'] as $prop) {
                                $this->goods[$need][$prop] = $f['ID'];
                            }
                        } else {
                            $this->goods[$need][$p[$need]['VALUE']] = $f['ID'];
                        }
                    }
                }
                if ($p['ARTICUL_LG']['VALUE']) {
                    foreach($p['ARTICUL_LG']['VALUE'] as $val) {
                        if (stripos($val, '***')!==false) {
                            $this->vvars[str_ireplace('***', '', $val)] = $f['ID'];
                        } else {
                            $this->vars[$val] = $f['ID'];
                        }
                    }
                }
            }
        }
    }

    /**
     * Обновляет доступность и остатки товара по его ID
     * @param $id - ID товара
     * @param $q - количество
     */
    protected function quantityUpdate(int $id, int $q)
    {
        $rsStoreProduct = \Bitrix\Catalog\StoreProductTable::getList([
            'filter' => ['=PRODUCT_ID'=>$id,'STORE.ACTIVE'=>'Y'],
        ]);
        $goodIsExist = false;
        $total = $q;
        while($arStoreProduct=$rsStoreProduct->fetch()) {
            if ($arStoreProduct['STORE_ID']==$this->storeID) {
                $goodIsExist = true;
                \Bitrix\Catalog\StoreProductTable::update($arStoreProduct['ID'], ['PRODUCT_ID' => $id,'STORE_ID' => $this->storeID,'AMOUNT' => $q]);
            } else {
                $total += $arStoreProduct['AMOUNT'];
            }
        }
        if (!$goodIsExist) {
            \Bitrix\Catalog\StoreProductTable::add(['PRODUCT_ID' => $id,'STORE_ID' => $this->storeID,'AMOUNT' => $q]);
        }
        $this->ids[] = $id;
        \Bitrix\Catalog\ProductTable::update($id, ['QUANTITY' => $total, 'AVAILABLE' => ($q>0)?'Y':'N']);
    }

    /**
     * Обнуляет остатки текущего склада по необновленным позициям
     */
    protected function clearRests()
    {
        $rsStoreProduct = \Bitrix\Catalog\StoreProductTable::getList([
            'filter' => ['!PRODUCT_ID' => $this->ids,'STORE_ID' => $this->storeID],
        ]);
        $products = [];
        $amounts = [];
        while($arStoreProduct=$rsStoreProduct->fetch()) {
            $products[] = $arStoreProduct['PRODUCT_ID'];
            $amounts[$arStoreProduct['PRODUCT_ID']] = $arStoreProduct['AMOUNT'];
            \Bitrix\Catalog\StoreProductTable::update($arStoreProduct['ID'], ['PRODUCT_ID' => $arStoreProduct['PRODUCT_ID'],'STORE_ID' => $this->storeID, 'AMOUNT' => 0]);
        }
        $productsDb = \Bitrix\Catalog\ProductTable::getList([
            'filter' => ['ID'=>$products]
        ]);
        while($product=$productsDb->fetch()) {
            $q = $product['QUANTITY']-$amounts[$product['ID']];
            $total = ($q>0)?$q:0;
            \Bitrix\Catalog\ProductTable::update($product['ID'], ['QUANTITY' => $total, 'AVAILABLE' => ($total > 0) ? 'Y' : 'N']);
        }
    }
}