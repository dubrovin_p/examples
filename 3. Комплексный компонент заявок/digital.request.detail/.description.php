<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => GetMessage("DIGITAL_ANALYTICS_DETAIL"),
    "DESCRIPTION" => GetMessage("DIGITAL_ANALYTICS_DETAIL_DESCRIPTION"),
    "ICON" => "/images/digital.gif",
    "COMPLEX" => "Y",
    "PATH" => array(
        "ID" => "content",
        "CHILD" => array(
            "ID" => "digital_detail",
            "NAME" => GetMessage("DIGITAL_ANALYTICS_DETAIL"),
            "SORT" => 100,
        ),
    ),
);

?>