<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?if ($arResult["ERRORS"]) {
    var_export($arResult["ERRORS"]);
} else {?>
    <div class="b-iex-form">
        <form class="js-add-request-form" action="<?=$APPLICATION->GetCurDir()?>" method="POST" enctype="multipart/form-data">
            <?=bitrix_sessid_post()?>
            <table class="data-table iex-research" width="100%" cellspacing="0" cellpadding="0" border="0">
                <tbody>
                    <? foreach ($arResult["TASK"]["FIELDS"] as $key => $field) { 
                        $required = false;
                        if ($field["PROPERTIES"]["REQUIRED"]["VALUE"]) {
                            $required = true;
                        }
                        $multiple = false;
                        if ($field["PROPERTIES"]["MULTIPLE"]["VALUE"]) {
                            $multiple = true;
                        }
                        $placeholder = "";
                        if ($field["PROPERTIES"]["PLACEHOLDER"]["VALUE"]) {
                            $placeholder = $field["PROPERTIES"]["PLACEHOLDER"]["VALUE"];
                        } ?>
                        <tr>
                            <td><?=$field["NAME"]?><?=($required)?' <span class="starrequired">*</span>':'';?></td>
                            <td>
                                <? switch ($field["PROPERTIES"]["TYPE"]["VALUE_XML_ID"]) {
                                    case "text": ?>
                                        <textarea cols="100" rows="6" name="PROPERTY[<?=$field["CODE"]?>]"><?=$placeholder?></textarea>
                                        <? break;
                                    case "radio":
                                    case "checkbox":
                                        foreach ($field["PROPERTIES"]["LIST"]["VALUE"] as $key => $LIST) { 
                                            $class = "prop".$field["CODE"]; ?>
                                            <div class="b-checkbox">
                                                <input class="<?=$class?>" type="<?=$field["PROPERTIES"]["TYPE"]["VALUE_XML_ID"]?>" name="PROPERTY[<?=$field["CODE"]?>][]" value="<?=$field["PROPERTIES"]["LIST"]["PROPERTY_VALUE_ID"][$key]?>" id="<?=$field["CODE"]?>_<?=$key?>" <?=($required)?' required onclick=\'deRequire("'.$class.'")\'':''?>>
                                                <i class="b-checkbox__ico"></i><label for="<?=$field["CODE"]?>_<?=$key?>"><?=$LIST?></label>
                                            </div>
                                        <? }
                                        break;
                                    case "select": ?>
                                        <select class="b-custom-select" name="PROPERTY[<?=$field["CODE"]?>][]"<?=($required)?' required':'';?><?=($multiple)?' multiple':'';?>>
                                            <option></option>
                                            <? foreach ($field["PROPERTIES"]["LIST"]["VALUE"] as $key => $LIST) { ?>
                                                <option value="<?=$field["PROPERTIES"]["LIST"]["PROPERTY_VALUE_ID"][$key]?>"><?=$LIST?></option>
                                            <? } ?>
                                        </select>
                                        <?break;
                                    case "string_select":
                                    case "calendar2":
                                    case "string_calendar": 
                                        $type = "text";
                                        if ($field["PROPERTIES"]["TYPE"]["VALUE_XML_ID"] == "calendar2") {
                                            $type = "date";
                                        } ?>
                                        <div class="fieldBlock multifield">
                                            <div class="<?=($multiple)?'additional':''?>">
                                                <input type="<?=$type?>" name="PROPERTY[<?=$field["CODE"]?>][text][]" size="30" value="" <?=($required)?' required':'';?> placeholder="<?=$placeholder?>">
                                                <? if ($field["PROPERTIES"]["TYPE"]["VALUE_XML_ID"] == "string_select") { ?>
                                                    <select class="" name="PROPERTY[<?=$field["CODE"]?>][data][]"<?=($required)?' required':'';?>>
                                                        <option></option>
                                                        <? foreach ($field["PROPERTIES"]["LIST"]["VALUE"] as $key => $LIST) { ?>
                                                            <option value="<?=$field["PROPERTIES"]["LIST"]["PROPERTY_VALUE_ID"][$key]?>"><?=$LIST?></option>
                                                        <? } ?>
                                                    </select>
                                                <? } else { ?>
                                                    <input type="date" name="PROPERTY[<?=$field["CODE"]?>][data][]" size="30" value="" <?=($required)?' required':'';?> placeholder="<?=$placeholder?>">
                                                <? }?>
                                                <? if ($multiple) { ?>
                                                    <button class="delField"></button>
                                                <? } ?>
                                            </div>
                                        </div>
                                        <? if ($multiple) { ?>
                                            <div>
                                                <button class="moreFields">Добавить</button>
                                            </div>
                                        <? } ?>
                                        <? break;
                                    case "user":?>
                                        <?$APPLICATION->IncludeComponent(
                                        'bitrix:main.user.selector',
                                        '',
                                        [
                                           "ID" => $field["CODE"],
                                           "API_VERSION" => 3,
                                           "LIST" => [],
                                           "INPUT_NAME" => "PROPERTY[".$field["CODE"]."]",
                                           "USE_SYMBOLIC_ID" => false,
                                           "BUTTON_SELECT_CAPTION" => "",
                                           "SELECTOR_OPTIONS" => 
                                            [
                                              "departmentSelectDisable" => "Y",
                                              'context' => $field["CODE"],
                                              'contextCode' => 'U',
                                              'enableAll' => 'N',
                                              'userSearchArea' => 'I'
                                            ]
                                        ]
                                        );?>
                                        <?break;
                                    case "calendar":
                                    case "email":
                                    case "string":
                                    case "file":
                                    default: 
                                        $type = "text";
                                        if ($field["PROPERTIES"]["TYPE"]["VALUE_XML_ID"]=="calendar") {
                                            $type = "date";
                                        } elseif ($field["PROPERTIES"]["TYPE"]["VALUE_XML_ID"]=="email") {
                                            $type = "email";
                                        } elseif ($field["PROPERTIES"]["TYPE"]["VALUE_XML_ID"]=="file") {
                                            $type = "file";
                                        }?>
                                        <div class="fieldBlock">
                                            <div class="<?=($multiple)?'additional':''?>">
                                                <input type="<?=$type?>" name="PROPERTY[<?=$field["CODE"]?>]<?=($multiple)?'[]':''?>" size="30" value="" <?=($required)?' required':'';?> placeholder="<?=$placeholder?>">
                                                <? if ($multiple) { ?>
                                                    <button class="delField"></button>
                                                <? } ?>
                                            </div>
                                        </div>
                                        <? if ($multiple) { ?>
                                            <div>
                                                <button class="moreFields">Добавить</button>
                                            </div>
                                        <? } ?>
                                <? } ?>
                            </td>
                        </tr>
                    <? } ?> 
                    <tr>
                        <td style="border:none;">
                            <input class="b-btn b-btn_grey" type="button" value="Назад" onclick="window.location='<?=$arResult["TASK"]["BACK_URL"]?>';return false;">
                            <input type="submit" value="Сохранить" name="next" class="b-btn">
                        </td>
                    </tr>
                </tbody>
            </table>
        </form>
    </div>
    <script>
        $(document).ready(function(){
            $(document).on("click", ".moreFields", function(e){
                e.preventDefault();
                $(this).closest("td").find(".additional").last().clone().appendTo($(this).closest("td").find(".fieldBlock"));
                //$(this).closest("td").find(".additional").last().find(".b-custom-select").select2();
            });
            $(document).on("click", ".delField", function(e){
                e.preventDefault();
                 if ($(this).closest(".fieldBlock").find(".additional").length > 1)
                    $(this).closest(".additional").remove();
            });
        });
        function deRequire(elClass) {
            el=document.getElementsByClassName(elClass);

            var atLeastOneChecked=false;//at least one cb is checked
            for (i=0; i<el.length; i++) {
                if (el[i].checked === true) {
                    atLeastOneChecked=true;
                }
            }

            if (atLeastOneChecked === true) {
                for (i=0; i<el.length; i++) {
                    el[i].required = false;
                }
            } else {
                for (i=0; i<el.length; i++) {
                    el[i].required = true;
                }
            }
        }
    </script>
    <style>
        .additional {
            position: relative;
            height: 32px;
            margin-bottom: 5px;
        }
        .additional button {
            position: absolute;
        }
        .moreFields {
            border: none;
            border-bottom: 1px dashed #2067b0;
            margin-top: 5px;
            display: inline-block;
            color: #2067b0;
            background: none;
        }
        .delField {
            border: none;
            background: url(/bitrix/js/tasks/css/images/media.png) no-repeat 0 -15px;
            display: inline-block;
            position: absolute;
            width: 10px;
            height: 10px;
            margin-left: 10px;
            cursor: pointer;
            top: 10px;
        }
        @media (min-width: 769px) {
            .multifield input, .multifield select {
                max-width: 47%!important;
                margin-right: 2%;
            }
        }
        .b-iex-form input[type="email"], .b-iex-form input[type="file"] {
            background: #FAFAFA;
            border: 1px solid #D0D8DE;
            box-sizing: border-box;
            border-radius: 4px;
            height: 32px;
            padding: 0 10px;
            display: inline-flex;
            align-items: center;
            width: 100%;
            max-width: 379px;
            font-size: 14px;
        }
        .b-iex-form input[type="file"] {
            background: none;
            border: none;
        }
    </style>
<?}?>