<?php

use \Bitrix\Main\Loader;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

class RequestDetailComponent extends CBitrixComponent
{
    const DEFAULT_CACHE_TIME = 3600;

    private $lists = [];
    private $required = [];
    private $fieldsName = [];
    private $description = "";
    private $auditors = [];
    private $saveData = [];
    private $files = [];

    public function executeComponent()
    {
        Loader::includeModule('iblock');
        /*if ($this->startResultCache($this->getCacheTime())) {*/
            $this->fetchTask();
            $this->fetchFields();
        //}
        if ($_REQUEST["PROPERTY"]) {
            if (check_bitrix_sessid()) {
                $this->auditors = [$this->arResult["TASK"]["UF_OBSERVER"]];
                $this->getFieldsFromDB();
                if ($this->getTaskDescription()) {
                    $this->addTask();
                }
            } else {
                $this->arResult["ERRORS"][] = GetMessage("SESSION_ERROR");
            }
        }
        global $APPLICATION;
        $APPLICATION->SetTitle($this->arResult["TASK"]["NAME"]);
        $APPLICATION->SetPageProperty("title", $this->arResult["TASK"]["NAME"]);
        $this->includeComponentTemplate();
    }

    private function getCacheTime(): int
    {
        if (!empty($this->arParams['CACHE_TIME'])) {
            return intval($this->arParams['CACHE_TIME']);
        }
        return self::DEFAULT_CACHE_TIME;
    }

    private function fetchTask()
    {
        $rows = CIBlockSection::GetList(
            ["SORT" => "ASC", "NAME" => "ASC"],
            ["IBLOCK_ID" => $this->arParams["IBLOCK_ID"], "ACTIVE" => "Y", "ID" => $this->arParams["ID"]],
            false,
            ["ID", "NAME", "CODE", "SECTION_PAGE_URL", "UF_*", "IBLOCK_SECTION_ID"]
        );
        if ($row = $rows->GetNext()) {
            if ($row["IBLOCK_SECTION_ID"]) {
                $rowsParent = CIBlockSection::GetList(
                    ["SORT" => "ASC", "NAME" => "ASC"],
                    ["IBLOCK_ID" => $this->arParams["IBLOCK_ID"], "ACTIVE" => "Y", "ID" => $row["IBLOCK_SECTION_ID"]],
                    false,
                    ["ID", "NAME", "SECTION_PAGE_URL"]
                );
                if ($rowParent = $rowsParent->GetNext()) {
                    $row["BACK_URL"] = $rowParent["SECTION_PAGE_URL"];
                }
            }
            if (!$row["BACK_URL"]) {
                $row["BACK_URL"] = $this->arParams["FOLDER"];
            }
            $this->arResult["TASK"] = $row;
        } else {
            $this->arResult["ERRORS"][] = GetMessage("NO_TASK");
        }
    }

    private function fetchFields()
    {
        $this->arResult["TASK"]["FIELDS"] = [];
        $res = CIBlockElement::GetList([], ["IBLOCK_ID" => $this->arParams["IBLOCK_ID"], "ACTIVE" => "Y", "SECTION_ID" => $this->arParams["ID"]]);
        while ($el = $res->GetNextElement()) {
            $field = $el->GetFields();
            $field["PROPERTIES"] = $el->GetProperties();
            $this->arResult["TASK"]["FIELDS"][$field["CODE"]] = $field;
        }
    }

    private function getFieldsFromDB()
    {
        foreach($this->arResult["TASK"]["FIELDS"] as $field) {
            $this->fieldsName[strtolower($field["CODE"])] = $field["NAME"];
            if ($field["PROPERTIES"]["REQUIRED"]["VALUE"]) {
                $this->required[strtolower($field["CODE"])] = strtolower($field["CODE"]);
            }
            if ($field["PROPERTIES"]["LIST"]["VALUE"]) {
                foreach ($field["PROPERTIES"]["LIST"]["VALUE"] as $key => $listVal) {
                    $this->lists[strtolower($field["CODE"])][$field["PROPERTIES"]["LIST"]["PROPERTY_VALUE_ID"][$key]] = $listVal;
                }
            }
        }
    }

    private function getTaskDescription()
    {
        foreach($_REQUEST["PROPERTY"] as $code=>$value) {
            if (!$value) {
                if ($this->required[$code]) {
                    $this->arResult["ERRORS"][] = GetMessage("EMPTY");
                    return false;
                }
                continue;
            }
            if ($this->required[$code]) {
                unset($this->required[$code]);
            }
            if ($this->arResult["TASK"]["FIELDS"][$code]["PROPERTIES"]["TYPE"]["VALUE_XML_ID"] == "user") {
                $this->auditors[] = htmlspecialchars($value);
                continue;
            }
            $this->description .= "<b>".$this->fieldsName[$code]."</b>: ";
            if (is_array($value)) {
                if (isset($value["text"]) && isset($value["data"])) {
                    foreach($value["text"] as $idVal=>$val) {
                        if (!$val) continue;
                        $this->saveData[$code][$idVal] = htmlspecialchars($val);
                        $this->description .=  htmlspecialchars($val)." - ";
                        if ($this->lists[$code]) {
                            $this->description .=$this->lists[$code][$value["data"][$idVal]]."<br/>";
                        } else {
                            $this->description .= htmlspecialchars($value["data"][$idVal])."<br/>";
                        }
                    }
                } else {
                    foreach($value as $idVal=>$val) {
                        $this->saveData[$code][$idVal] = htmlspecialchars($val);
                        if ($this->lists[$code]) {
                            $this->description .=$this->lists[$code][$val]."<br/>";
                        } else {
                            $this->description .= $this->saveData[$code][$idVal]."<br/>";
                        }
                    }
                }
            } else {
                $this->saveData[$code] = htmlspecialchars($value);
                if ($this->lists[$code]) {
                    $this->description .=$this->lists[$code][$value]."<br/>";
                } else {
                    $this->description .= $this->saveData[$code]."<br/>";
                }
            }
            $this->description .= "<br/>";
        }
        if ($_FILES) {
            foreach($_FILES["PROPERTY"]["name"] as $code=>$prop) {
                foreach($prop as $i=>$file) {
                    if (!$file) {
                        if ($this->required[$code]) {
                            $this->arResult["ERRORS"][] = GetMessage("EMPTY");
                            return false;
                        }
                        continue;
                    }
                    if ($this->required[$code]) {
                        unset($this->required[$code]);
                    }
                    $arFile = [
                        "name" => $file,
                        "size" => $_FILES["PROPERTY"]["size"][$code][$i],
                        "tmp_name" => $_FILES["PROPERTY"]["tmp_name"][$code][$i],
                        "type" => $_FILES["PROPERTY"]["type"][$code][$i],
                        "old_file" => false,
                        "del" => "N",
                        "MODULE_ID" => "tasks"
                    ];
                    $storage = Bitrix\Disk\Driver::getInstance()->getStorageByUserId($this->arResult["TASK"]["UF_RESPONSIBLE"]);
                    $folder = $storage->getFolderForUploadedFiles();
                    $file = $folder->uploadFile($arFile, array(
                       'NAME' => $arFile["name"],
                       'CREATED_BY' => $this->arResult["TASK"]["UF_RESPONSIBLE"]
                    ), array(), true);
                    $FILE_ID = $file->getId();
                    $this->files[] = "n$FILE_ID";
                }
            }
        }
        var_dump($this->required);
        if ($this->required) {
            $this->arResult["ERRORS"][] = GetMessage("EMPTY");
            return false;
        }
        return true;
    }

    private function addTask()
    {
        Loader::includeModule('tasks');
        $task = new \Bitrix\Tasks\Item\Task();
        $task["TITLE"] = $this->arResult["TASK"]["NAME"]." - ".$this->saveData["client"];
        $task["DESCRIPTION"] = $this->description;
        $task["RESPONSIBLE_ID"] = $this->arResult["TASK"]["UF_RESPONSIBLE"];
        $task["AUDITORS"] = $this->auditors;
        $task["GROUP_ID"] = $this->arResult["TASK"]["UF_GROUP"];
        if ($this->files) {
            $task["UF_TASK_WEBDAV_FILES"] = $this->files;
        }
        $result = $task->save();
        if($result->isSuccess()) {
            $ID = $task->getID();
            LocalRedirect("/workgroups/group/{$this->arResult['TASK']['UF_GROUP']}/tasks/task/view/{$ID}/");
        } else {
            $this->arResult["ERRORS"][] = GetMessage("TASK_ERROR");
        }
    }
}