<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="wizard b-iex-form">
    <table class="data-table" width="100%" cellspacing="0" cellpadding="0" border="0">
        <tbody>
            <tr>
                <td style="border:none; text-align: left;">
                    <div class="js-legal-step1">
                        <? foreach ($arResult["TASKS"] as $id => $task) { ?>
                            <div class="wizard_sections b-checkbox">
                                <input type="radio" name="LEGAL_REQUEST" value="<?=$task["SECTION_PAGE_URL"]?>" id="task<?=$id?>" <?=($id===0)?"checked":""?>>
                                <i class="b-checkbox__ico"></i>
                                <label for="task<?=$id?>"><span class="text"><?=$task["NAME"]?></span></label>
                            </div>
                        <? } ?>
                    </div>
                </td>
            </tr>
            <tr>
                <td style="border:none;">
                    <input type="submit" value="Далее" name="next" class="b-btn js-legal-next">
                </td>
            </tr>
        </tbody>
    </table>
</div>