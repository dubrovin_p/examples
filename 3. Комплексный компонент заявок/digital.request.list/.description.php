<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => GetMessage("DIGITAL_ANALYTICS_LIST"),
    "DESCRIPTION" => GetMessage("DIGITAL_ANALYTICS_LIST_DESCRIPTION"),
    "ICON" => "/images/digital.gif",
    "COMPLEX" => "N",
    "PATH" => array(
        "ID" => "content",
        "CHILD" => array(
            "ID" => "digital_list",
            "NAME" => GetMessage("DIGITAL_ANALYTICS_LIST"),
            "SORT" => 100,
        ),
    ),
);

?>