<?php

use \Bitrix\Main\Loader;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

class RequestListComponent extends CBitrixComponent
{
    const DEFAULT_CACHE_TIME = 3600;

    public function executeComponent()
    {
        Loader::includeModule('iblock');

        $cacheTime = isset($this->arParams['CACHE_TIME']) ? intval($this->arParams['CACHE_TIME']) : $this::DEFAULT_CACHE_TIME;

        if ($this->startResultCache($cacheTime)) {
            $rows = CIBlockSection::GetList(
                ["SORT" => "ASC", "NAME" => "ASC"],
                ["IBLOCK_ID" => $this->arParams["IBLOCK_ID"], "ACTIVE" => "Y", "SECTION_ID" => $this->arParams["ID"]],
                false,
                ["NAME", "CODE", "SECTION_PAGE_URL", "UF_*"]
            );
            while ($row = $rows->GetNext()) {
                $this->arResult["TASKS"][] = $row;
            }
            $this->includeComponentTemplate();
        }
    }
}