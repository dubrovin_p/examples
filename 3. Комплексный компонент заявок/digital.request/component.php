<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
/** @global CUser $USER */
/** @global CMain $APPLICATION */

$arDefaultUrlTemplates404 = [
    "tasks" => "#SECTION_CODE_PATH#/",
    "task" => "#SECTION_CODE_PATH/",
];

$arDefaultVariableAliases404 = [];

$arDefaultVariableAliases = [];

$arComponentVariables = ["SECTION_CODE"];

$arVariables = [];

$arUrlTemplates = CComponentEngine::makeComponentUrlTemplates($arDefaultUrlTemplates404, []);
$arVariableAliases = CComponentEngine::makeComponentVariableAliases($arDefaultVariableAliases404, []);

$engine = new CComponentEngine($this);
if (CModule::IncludeModule('iblock'))
{
    $engine->addGreedyPart("#SECTION_CODE_PATH#");
    $engine->setResolveCallback(array("CIBlockFindTools", "resolveComponentEngine"));
}
$componentPage = $engine->guessComponentPath(
    $arParams["SEF_FOLDER"],
    $arUrlTemplates,
    $arVariables
);
$b404 = false;
if(!$componentPage)
{
    $componentPage = "tasks";
    $b404 = true;
}

if($componentPage == "task")
{
    $b404 |= !isset($arVariables["SECTION_CODE"]);
}

if($b404 && CModule::IncludeModule('iblock'))
{
    $folder404 = str_replace("\\", "/", $arParams["SEF_FOLDER"]);
    if ($folder404 != "/")
        $folder404 = "/".trim($folder404, "/ \t\n\r\0\x0B")."/";
    if (mb_substr($folder404, -1) == "/")
        $folder404 .= "index.php";

    if ($folder404 != $APPLICATION->GetCurPage(true))
    {
        \Bitrix\Iblock\Component\Tools::process404("", "Y", "Y", "Y", "/404.php");
    }
}

CComponentEngine::initComponentVariables($componentPage, $arComponentVariables, $arVariableAliases, $arVariables);
$arResult = [
    "FOLDER" => $arParams["SEF_FOLDER"],
    "URL_TEMPLATES" => $arUrlTemplates,
    "VARIABLES" => $arVariables,
    "ALIASES" => $arVariableAliases,
];

if ($arVariables["SECTION_CODE"]) {
    $rows = CIBlockSection::GetList(
        ["SORT" => "ASC", "NAME" => "ASC"],
        ["IBLOCK_ID" => $arParams["IBLOCK_ID"], "ACTIVE" => "Y", "CODE" => $arVariables["SECTION_CODE"]],
        false,
        ["ID"]
    );
    if ($sect = $rows->GetNext()) {
        $res = CIBlockElement::GetList([], ["IBLOCK_ID" => $arParams["IBLOCK_ID"], "ACTIVE" => "Y", "SECTION_ID" => $sect["ID"]]);
        if ($res->SelectedRowsCount()) {
            $componentPage = "task";
        }
    } else {
        \Bitrix\Iblock\Component\Tools::process404("", "Y", "Y", "Y", "/404.php");
    }
} else {
    \Bitrix\Iblock\Component\Tools::process404("", "Y", "Y", "Y", "/404.php");
}
$this->includeComponentTemplate($componentPage);