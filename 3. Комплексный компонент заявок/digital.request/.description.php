<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => GetMessage("DIGITAL_ANALYTICS"),
    "DESCRIPTION" => GetMessage("DIGITAL_ANALYTICS_DESCRIPTION"),
    "ICON" => "/images/digital.gif",
    "COMPLEX" => "Y",
    "PATH" => array(
        "ID" => "content",
        "CHILD" => array(
            "ID" => "digital",
            "NAME" => GetMessage("DIGITAL_ANALYTICS"),
            "SORT" => 100,
        ),
    ),
);

?>